var cars = ["Saab", "Volvo", "BMW"]

console.log("1.Get cars")
GetCars(cars)

console.log("2.Get BMW value")
console.log(cars[2])

console.log("3.Change first item")
cars[0]="Changed"
GetCars(cars)

console.log("4.Remove last item in the array")
cars.pop(cars.length)
GetCars(cars)

console.log("5.Add Audi")
cars.push("Audi")
GetCars(cars)

console.log("6.Splice Volvo and BMW")
cars.splice(1,2)
GetCars(cars)

function GetCars(cars){
    cars.forEach(element => {
        console.log(element)
    });
}